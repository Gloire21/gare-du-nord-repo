package com.garedunord.ticketservice.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    OpenAPI api() {
        return new OpenAPI()
                .info(new Info()
                        .title("Ticket API")
                        .description("Pour voyager dans differente partie de la ville, nous empreintons differents moyens de transport." +
                                "Ticket service est pour faciler la tache aux voyageurs de se procurer un Ticket. ")
                        .contact(new Contact().email("gloire2007@hotmail.fr").url("github.com/brenn21"))
                        .version("V.1.0.0")
                        .license(new License().name("Moscow License"))
                );
    }
}
